package com.microservico.exemplo.hrpayroll.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Worker implements Serializable {

	private static final long serialVersionUID = -7680739084056197972L;
	
	private Long Id;
	private String name;
	private Double dailyIncome;
	
}
