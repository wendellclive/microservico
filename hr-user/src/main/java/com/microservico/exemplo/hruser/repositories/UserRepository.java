package com.microservico.exemplo.hruser.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.microservico.exemplo.hruser.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

	User findByEmail(String email);
	
}
